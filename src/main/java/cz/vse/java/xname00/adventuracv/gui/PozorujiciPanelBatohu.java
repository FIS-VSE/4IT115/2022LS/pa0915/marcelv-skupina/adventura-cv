package cz.vse.java.xname00.adventuracv.gui;

import cz.vse.java.xname00.adventuracv.logika.Batoh;
import cz.vse.java.xname00.adventuracv.logika.Hra;
import cz.vse.java.xname00.adventuracv.logika.IHra;
import cz.vse.java.xname00.adventuracv.observer.Observer;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;

import java.util.Set;

public class PozorujiciPanelBatohu implements Observer {

    private final IHra hra = Hra.getSingleton();
    private final Batoh batoh = hra.getBatoh();
    private final VBox vBox = new VBox();
    private final FlowPane flowPane = new FlowPane();
    private final Label label = new Label("Věci v inventáři:");

    public PozorujiciPanelBatohu() {
        batoh.addObserver(this);

        vBox.getChildren().addAll(label, flowPane);
    }

    public VBox getVBox() {
        return vBox;
    }

    @Override
    public void update() {
        Set<String> predmety = batoh.getMnozinaVeci();
        for (String predmet : predmety) {
            Image image = new Image(PozorujiciPanelBatohu.class.getResourceAsStream(
                    "/zdroje/" + predmet + ".jpeg"
            ), 150.0, 150.0, false, false);
            ImageView imageView = new ImageView(image);
            flowPane.getChildren().add(imageView);
        }
    }
}
