package cz.vse.java.xname00.adventuracv.gui;

import cz.vse.java.xname00.adventuracv.logika.HerniPlan;
import cz.vse.java.xname00.adventuracv.logika.Hra;
import cz.vse.java.xname00.adventuracv.logika.IHra;
import cz.vse.java.xname00.adventuracv.logika.Prostor;
import cz.vse.java.xname00.adventuracv.main.AdventuraZaklad;
import cz.vse.java.xname00.adventuracv.observer.Observer;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

public class PozorujiciPlanekHry implements Observer {

    private AnchorPane planekHry = new AnchorPane();
    private IHra hra = Hra.getSingleton();
    // private Circle tecka = new Circle(10.0, Paint.valueOf("red"));
    private final ImageView viewKarkulky;
    private HerniPlan herniPlan;

    public PozorujiciPlanekHry(ImageView viewKarkulky) {
        this.viewKarkulky = viewKarkulky;

        herniPlan = hra.getHerniPlan();
        herniPlan.addObserver(this);

        System.out.println("Vypisuji this: " + this);

        Image planekHryImage = new Image(
                AdventuraZaklad.class.getResourceAsStream("/zdroje/herniPlan.png"),
                400.0, 250.0, false, false
        );
        ImageView planekHryView = new ImageView(planekHryImage);


        planekHry.getChildren().addAll(planekHryView, viewKarkulky);
        update();
    }

    public AnchorPane getPlanekHry() {
        return planekHry;
    }

    @Override
    public void update() {
        if (viewKarkulky != null) {
            Prostor aktualniProstor = herniPlan.getAktualniProstor();
            AnchorPane.setLeftAnchor(viewKarkulky, aktualniProstor.getX());
            AnchorPane.setTopAnchor(viewKarkulky, aktualniProstor.getY());
            System.out.println(this.getClass() + ": volana metoda update() na objektu " + this);
            System.out.println("Aktualní prostor je " + aktualniProstor);
        }
    }
}
