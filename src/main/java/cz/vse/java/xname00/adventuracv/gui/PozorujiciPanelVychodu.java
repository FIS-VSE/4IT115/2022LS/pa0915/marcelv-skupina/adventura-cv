package cz.vse.java.xname00.adventuracv.gui;

import cz.vse.java.xname00.adventuracv.logika.HerniPlan;
import cz.vse.java.xname00.adventuracv.logika.Hra;
import cz.vse.java.xname00.adventuracv.logika.IHra;
import cz.vse.java.xname00.adventuracv.logika.Prostor;
import cz.vse.java.xname00.adventuracv.observer.Observer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;

import java.util.Collection;

public class PozorujiciPanelVychodu implements Observer {

    IHra hra = Hra.getSingleton();
    HerniPlan plan = hra.getHerniPlan();
    // logika - SubjectOfChange
    ObservableList<String> observableList = FXCollections.observableArrayList();
    // prezentace - Observer / posluchač / odběratel změn
    ListView<String> listView = new ListView<>();

    public PozorujiciPanelVychodu() {
        plan.addObserver(this);

        listView.setItems(observableList);

        update();
    }

    public ListView<String> getListView() {
        return listView;
    }

    @Override
    public void update() {
        Prostor aktualniProstor = plan.getAktualniProstor();
        Collection<Prostor> vychody = aktualniProstor.getVychody();
        observableList.clear();
        for (Prostor prostor : vychody) {
            observableList.add(prostor.getNazev());
        }

    }
}
