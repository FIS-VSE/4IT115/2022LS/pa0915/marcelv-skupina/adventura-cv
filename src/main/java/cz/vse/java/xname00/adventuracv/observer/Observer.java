package cz.vse.java.xname00.adventuracv.observer;

public interface Observer {

    void update();

}
